﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PutaBida
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Ahora toca hacer un conversor de celsius a farenheit
            /// La formula es "F = (C*1.8000)+32"
            /// Tengo que pedir la temperatura del C y que con eso saque el F 

            int cel, far;
            string temp;

            Console.WriteLine("Hola tu, ahora te voy a cambiar los grados de Celsius a Farenheit");
            Console.Write("A ver, que temperatura hace en tu casa ahora? ");
            temp = Console.ReadLine();
            cel = Convert.ToInt32(temp);
            Console.WriteLine("La temperatura es {0}", temp);

            far = Convert.ToInt32((cel * 1.8000f) + 32);

            Console.WriteLine("Entonces en farenheit ahora mismo seria {0}", far);
            Console.WriteLine("Okei, guay, pues agur");
        }        
    }
}
